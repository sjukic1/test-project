/* eslint-disable @typescript-eslint/no-var-requires */
const loadEnv = (env: string) => require("dotenv").config({ path: `${env}` });
loadEnv(`.env.${process.env.ENV_NAME}`);

export default {
  envName: process.env.ENV_NAME,
  nodeEnv: process.env.NODE_ENV,
  cors: {
    allowedOrigins: ["http://localhost:3000", "http://localhost:4000"],
  },
};
