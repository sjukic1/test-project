/* eslint-disable @typescript-eslint/no-var-requires */
const loadEnv = (env: string) => require("dotenv").config({ path: `${env}` });
loadEnv(`.env.${process.env.ENV_NAME}`);

export default {
  envName: process.env.ENV_NAME || "a",
  appUrl: "http://localhost:4000",
  serverPort: 4000,
  resolveEmptyAuthorization: false,
  frontendAppUrl: "http://localhost:3000",
  apiDocumentation: {
    isEnabled: true,
    suffix: "0001",
  },
  dbConfig: {
    type: "postgres",
    host: process.env.DATABASE_HOST || "localhost",
    port: process.env.DATABASE_PORT || "5435",
    username: process.env.DATABASE_USER || "root",
    password: process.env.DATABASE_PASSWORD || "root",
    database: process.env.DATABASE_DB || "test_project",
    logging: true,
  },
};
