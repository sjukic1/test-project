import {MigrationInterface, QueryRunner} from "typeorm";

export class createPermissionEntity1658787811764 implements MigrationInterface {
    name = 'createPermissionEntity1658787811764'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "permission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "code" text NOT NULL, "description" text NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_30e166e8c6359970755c5727a23" UNIQUE ("code"), CONSTRAINT "PK_3b8b97af9d9d8807e41e6f48362" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "permission_users_user" ("permissionId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_c03113665ef5a1c2258b47ad805" PRIMARY KEY ("permissionId", "userId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_a68535c371c96a600abe56090b" ON "permission_users_user" ("permissionId") `);
        await queryRunner.query(`CREATE INDEX "IDX_bc69d573e16cfac073d1a1aeed" ON "permission_users_user" ("userId") `);
        await queryRunner.query(`ALTER TABLE "permission_users_user" ADD CONSTRAINT "FK_a68535c371c96a600abe56090b7" FOREIGN KEY ("permissionId") REFERENCES "permission"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "permission_users_user" ADD CONSTRAINT "FK_bc69d573e16cfac073d1a1aeedc" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "permission_users_user" DROP CONSTRAINT "FK_bc69d573e16cfac073d1a1aeedc"`);
        await queryRunner.query(`ALTER TABLE "permission_users_user" DROP CONSTRAINT "FK_a68535c371c96a600abe56090b7"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_bc69d573e16cfac073d1a1aeed"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a68535c371c96a600abe56090b"`);
        await queryRunner.query(`DROP TABLE "permission_users_user"`);
        await queryRunner.query(`DROP TABLE "permission"`);
    }

}
