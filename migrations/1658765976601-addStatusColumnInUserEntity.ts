import {MigrationInterface, QueryRunner} from "typeorm";

export class addStatusColumnInUserEntity1658765976601 implements MigrationInterface {
    name = 'addStatusColumnInUserEntity1658765976601'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_status_enum" AS ENUM('Online', 'Offline', 'Vacationing', 'In a meeting', 'Out sick')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "status" "public"."user_status_enum" NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "status"`);
        await queryRunner.query(`DROP TYPE "public"."user_status_enum"`);
    }

}
