export interface ConfigApiDocumentation {
  isEnabled: boolean;
  suffix: string;
}
