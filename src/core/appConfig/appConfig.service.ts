import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ConfigApiDocumentation } from "./appConfig.interfaces";

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  public get env(): string {
    return this.configService.get("ENV_NAME");
  }

  public get appUrl(): string {
    return this.configService.get("appUrl");
  }

  public get serverPort(): number {
    return parseInt(this.configService.get("serverPort"), 10) || 4000;
  }

  public get dbConfig() {
    return {
      host: this.configService.get("dbConfig.host"),
      port: Number(this.configService.get("dbConfig.port")),
      username: this.configService.get("dbConfig.username"),
      password: this.configService.get("dbConfig.password"),
      database: this.configService.get("dbConfig.database"),
      ssl: this.configService.get("dbConfig.ssl"),
      cache: this.configService.get("dbConfig.cache"),
      logging: true,
    };
  }

  public get isProductionZone(): boolean {
    return ["p", "prd", "prod", "production", "u", "uat"].includes(this.env);
  }

  public get apiDocumentation(): ConfigApiDocumentation {
    return this.configService.get("apiDocumentation");
  }

  public get jwtKeys(): { secret: string; public: string } {
    return this.configService.get("authKeys");
  }

  public get frontend() {
    return {
      url: this.configService.get("frontendAppUrl"),
    };
  }

  public get cors() {
    return this.configService.get("cors");
  }
}
