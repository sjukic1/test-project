import { ConfigObject } from "@nestjs/config";
import a from "../../../config/a";
import defaultConfig from "../../../config/default";

export const CONFIGURATION_FILES: Record<string, ConfigObject> = {
  defaultConfig,
  a,
};
