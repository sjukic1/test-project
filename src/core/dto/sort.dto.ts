import { IsArray, IsOptional, IsString, Matches } from "class-validator";
import { Exclude, Expose, Transform } from "class-transformer";

@Exclude()
export class SortDto {
  @Expose()
  @Transform(({ value }) => {
    if (typeof value === "string") {
      return value.split(",");
    }
    return value || [];
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  @Matches(/^[+-]{0,1}[A-Za-z0-9_]+$/, { each: true })
  public sort: string[];
}
