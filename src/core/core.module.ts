import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppConfigService } from "./appConfig/appConfig.service";

@Module({
  imports: [ConfigModule, HttpModule, TypeOrmModule.forFeature([])],
  providers: [AppConfigService],
  exports: [AppConfigService, TypeOrmModule.forFeature([])],
})
export class CoreModule {}
