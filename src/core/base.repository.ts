import { AbstractRepository, SelectQueryBuilder, ObjectLiteral } from "typeorm";
import { validateOrReject } from "class-validator";
import { Pagination } from "../interfaces/pagination";
import { ISort } from "../interfaces/sort.interface";
import { IWhere } from "src/interfaces/filter.interface";

interface IQParams {
  pagination?: Pagination;
  sort?: ISort[];
  filter?: IWhere[];
}

export abstract class BaseRepository<T> extends AbstractRepository<T> {
  protected abstract tableName: string;

  protected entities: T[] = null;

  protected q(params?: IQParams): SelectQueryBuilder<T> {
    const qb = this.createQueryBuilder(this.tableName);

    if (params?.filter) {
      this.applyFilter(qb, params.filter);
    }
    if (params?.sort) {
      this.applySort(qb, params.sort);
    }
    if (params?.pagination) {
      this.applyPagination(qb, params.pagination);
    }
    return qb;
  }

  public async save(entity: T): Promise<T> {
    this.entities = null;
    if (typeof (entity as ObjectLiteral).validate === "function") {
      await (entity as ObjectLiteral).validate();
    } else {
      await validateOrReject(entity as unknown as object);
    }
    return this.manager.save(entity);
  }

  public async delete(entity: T): Promise<T> {
    this.entities = null;
    return this.manager.remove(entity);
  }

  protected applyPagination(q, pagination?: Pagination) {
    q.skip(pagination.pageSize * (pagination.page - 1)).take(
      pagination.pageSize
    );
    return q;
  }

  protected applySort(q: SelectQueryBuilder<T>, sort?: ISort[]) {
    if (sort) {
      sort.forEach((item) => q.addOrderBy(`"${item.property}"`, item.order));
    }
    return q;
  }

  protected applyFilter(q: SelectQueryBuilder<T>, filter?: IWhere[]) {
    if (filter) {
      filter.forEach((item, idx) => {
        q.andWhere(`${this.tableName}.${item.property} like :condition${idx}`, {
          ["condition" + idx]: `%${item.condition}%`,
        });
      });
    }
    return q;
  }
}
