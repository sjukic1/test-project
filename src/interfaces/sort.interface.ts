export interface ISort {
  property: string;
  order?: "ASC" | "DESC";
  nulls?: "NULLS FIRST" | "NULLS LAST";
}
