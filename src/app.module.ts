import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppConfigService } from "./core/appConfig/appConfig.service";
import { CONFIGURATION_FILES } from "./core/appConfig/configImports";
import { CoreModule } from "./core/core.module";
import { UserModule } from "./modules/user/user.module";
import { PermissionModule } from "./modules/permission/permission.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [
        () => CONFIGURATION_FILES.defaultConfig,
        () => CONFIGURATION_FILES[process.env.ENV_NAME],
      ],
      envFilePath: [`.env.${process.env.ENV_NAME}`],
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async (appConfig: AppConfigService) => {
        return {
          type: "postgres",
          ...appConfig.dbConfig,
          entities: [__dirname + "/**/*.entity{.ts,.js}"],
        };
      },
      imports: [CoreModule],
      inject: [AppConfigService],
    }),
    CoreModule,
    UserModule,
    PermissionModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
