import { EntityRepository } from "typeorm";
import { BaseRepository } from "../../core/base.repository";
import { IQParamsDTO } from "./dto/iQParamsDto";
import { User } from "./user.entity";

@EntityRepository(User)
export class UserRepository extends BaseRepository<User> {
  protected tableName = "user";

  async getUserByUsername(username: string) {
    return this.q().where("username = :username", { username }).getOne();
  }

  async getUserByEmail(email: string) {
    return this.q().where("email = :email", { email }).getOne();
  }

  async getUserById(id: string) {
    return this.q()
      .where("user.id = :id", { id })
      .leftJoinAndSelect("user.permissions", "permissions")
      .getOne();
  }

  async getAll(iQParams: IQParamsDTO): Promise<User[]> {
    return this.q(iQParams)
      .leftJoinAndSelect("user.permissions", "permissions")
      .getMany();
  }
}
