import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinColumn,
} from "typeorm";
import { Permission } from "../permission/permission.entity";
import { STATUS_ENUM } from "./user.enum";

@Entity("user")
export class User {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({ type: "text" })
  firstName: string;

  @Column({ type: "text" })
  lastName: string;

  @Column({ type: "text", unique: true })
  username: string;

  @Column({ type: "text" })
  password: string;

  @Column({ type: "text", unique: true })
  email: string;

  @Column({ type: "enum", enum: STATUS_ENUM })
  status: STATUS_ENUM;

  @ManyToMany(() => Permission, (permission) => permission.users, {
    nullable: true,
  })
  @JoinColumn()
  public permissions: Permission[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  constructor(data?: Partial<User>) {
    Object.assign(this, data);
  }
}
