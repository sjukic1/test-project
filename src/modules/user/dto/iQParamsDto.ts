import { IsNumber, IsOptional, IsString } from "class-validator";

export class PaginationDTO {
  @IsOptional()
  @IsNumber()
  pageSize?: number;

  @IsOptional()
  @IsNumber()
  page?: number;
}

export class ISortDTO {
  @IsString()
  property: string;

  @IsOptional()
  @IsString()
  order?: "ASC" | "DESC";

  @IsOptional()
  @IsString()
  nulls?: "NULLS FIRST" | "NULLS LAST";
}

export class IWhereDTO {
  @IsString()
  condition: string;

  @IsString()
  property: string;
}

export class IQParamsDTO {
  @IsOptional()
  pagination?: PaginationDTO;

  @IsOptional()
  sort?: ISortDTO[];

  @IsOptional()
  filter?: IWhereDTO[];
}
