export enum STATUS_ENUM {
  ONLINE = "Online",
  OFFLINE = "Offline",
  VACATIONING = "Vacationing",
  IN_A_MEETING = "In a meeting",
  OUT_SICK = "Out sick",
}
