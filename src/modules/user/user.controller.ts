import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  ValidationPipe,
} from "@nestjs/common";
import { CreatePermissionDto } from "../permission/dto/createPermissionDto";
import { CreateUserDto } from "./dto/createUserDto";
import { EditUserDto } from "./dto/editUserDto";
import { IQParamsDTO } from "./dto/iQParamsDto";
import { User } from "./user.entity";
import { UserService } from "./user.service";

@Controller("user")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get("/")
  async getAll(
    @Query(new ValidationPipe({ transform: true })) iQParams: IQParamsDTO
  ): Promise<User[]> {
    return this.userService.getAll(iQParams);
  }

  @Get("/:id")
  async getUserById(@Param("id") id: string): Promise<User> {
    return this.userService.getUserById(id);
  }

  @Post("/")
  async createUser(
    @Body(
      new ValidationPipe({
        skipMissingProperties: true,
        whitelist: true,
        transform: true,
      })
    )
    createuserDto: CreateUserDto
  ): Promise<User> {
    return this.userService.createUser(createuserDto);
  }

  @Patch("/:id")
  async editUser(
    @Param("id") id: string,
    @Body(
      new ValidationPipe({
        skipMissingProperties: true,
        whitelist: true,
        transform: true,
      })
    )
    editUserDto: EditUserDto
  ): Promise<User> {
    return this.userService.editUser(id, editUserDto);
  }

  @Patch("/:id/permissions")
  async createPermissionForUser(
    @Param("id") id: string,
    @Body(
      new ValidationPipe({
        skipMissingProperties: true,
        whitelist: true,
        transform: true,
      })
    )
    createPermission: CreatePermissionDto[]
  ): Promise<User> {
    return this.userService.createPermissionsForUser(id, createPermission);
  }

  @Delete("/:id")
  async deleteUser(@Param("id") id: string): Promise<{ success: boolean }> {
    return this.userService.deleteUser(id);
  }
}
