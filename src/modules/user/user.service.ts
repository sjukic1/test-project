import { HttpException, Injectable } from "@nestjs/common";
import { CreateUserDto } from "./dto/createUserDto";
import { User } from "./user.entity";
import * as bcrypt from "bcrypt";
import { UserRepository } from "./user.repository";
import { EditUserDto } from "./dto/editUserDto";
import { IQParamsDTO } from "./dto/iQParamsDto";
import { CreatePermissionDto } from "../permission/dto/createPermissionDto";
import { PermissionService } from "../permission/permission.service";
import { Permission } from "../permission/permission.entity";

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly permissionService: PermissionService
  ) {}

  async createUser(createUser: CreateUserDto): Promise<User> {
    const username = await this.userRepository.getUserByUsername(
      createUser.username
    );
    const email = await this.userRepository.getUserByEmail(createUser.email);
    if (username || email) {
      throw new HttpException(
        {
          status: 400,
          error: "Username or email already exits",
        },
        400
      );
    }
    return this.userRepository.save(
      new User({
        ...createUser,
        password: await bcrypt.hash(createUser.password, 12),
      })
    );
  }

  async editUser(id: string, editUser: EditUserDto): Promise<User> {
    const email = await this.userRepository.getUserByEmail(editUser.email);
    if (email) {
      throw new HttpException(
        {
          status: 400,
          error: "Email already exits",
        },
        400
      );
    }
    const user = await this.userRepository.getUserById(id);
    if (!user) {
      throw new HttpException(
        {
          status: 400,
          error: "User does not exits",
        },
        400
      );
    }
    const updatedUser = Object.assign(user, editUser);
    return this.userRepository.save(updatedUser);
  }

  async deleteUser(id: string): Promise<{ success: boolean }> {
    const user = await this.userRepository.getUserById(id);
    if (!user) {
      throw new HttpException(
        {
          status: 400,
          error: `User with id=${id} does not exits`,
        },
        400
      );
    }
    const deletedUser = await this.userRepository.delete(user);
    return deletedUser ? { success: true } : { success: false };
  }

  async getAll(iQParams: IQParamsDTO): Promise<User[]> {
    return this.userRepository.getAll(iQParams);
  }

  async createPermissionsForUser(
    id: string,
    createPermission: CreatePermissionDto[]
  ): Promise<User> {
    const user = await this.userRepository.getUserById(id);
    if (!user) {
      throw new HttpException(
        {
          status: 400,
          error: `User with id=${id} does not exits`,
        },
        400
      );
    }
    let permissions = user.permissions;

    permissions = permissions.filter((permission) => {
      return createPermission.find((item) => {
        return permission.code === item.code;
      });
    });

    for (const item of createPermission) {
      permissions.push(new Permission(item));
    }

    permissions = permissions.filter(
      (value, index, self) =>
        index === self.findIndex((t) => t.code === value.code)
    );

    const savePermission: Permission[] = [];
    for (const item of permissions) {
      const result = await this.permissionService.findOrCreatePermissionForUser(
        item
      );
      savePermission.push(result);
    }

    const updatedUser = Object.assign(user, {
      ...user,
      permissions: savePermission,
    });
    return this.userRepository.save(updatedUser);
  }

  async getUserById(id: string): Promise<User> {
    return this.userRepository.getUserById(id);
  }
}
