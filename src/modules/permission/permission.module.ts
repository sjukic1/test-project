import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CoreModule } from "src/core/core.module";
import { PermissionRepository } from "./permission.repository";
import { PermissionService } from "./permission.service";

@Module({
  imports: [CoreModule, TypeOrmModule.forFeature([PermissionRepository])],
  providers: [PermissionService],
  exports: [PermissionService],
  controllers: [],
})
export class PermissionModule {}
