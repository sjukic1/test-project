import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { User } from "../user/user.entity";

@Entity("permission")
export class Permission {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({ type: "text", unique: true })
  code: string;

  @Column({ type: "text" })
  description: string;

  @ManyToMany(() => User, (user) => user.permissions, {
    cascade: true,
    nullable: true,
  })
  @JoinTable()
  public users?: User[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  constructor(data?: Partial<Permission>) {
    Object.assign(this, data);
  }
}
