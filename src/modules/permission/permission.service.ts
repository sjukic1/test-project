import { Injectable } from "@nestjs/common";
import { User } from "../user/user.entity";
import { CreatePermissionDto } from "./dto/createPermissionDto";
import { Permission } from "./permission.entity";
import { PermissionRepository } from "./permission.repository";

@Injectable()
export class PermissionService {
  constructor(private readonly permissionRepository: PermissionRepository) {}

  async findOrCreatePermissionForUser(
    createPermission: Permission
  ): Promise<Permission> {
    let permisssion = await this.permissionRepository.getPermissionByCode(
      createPermission.code
    );
    if (!permisssion) {
      permisssion = await this.permissionRepository.save(
        new Permission(createPermission)
      );
    }
    return permisssion;
  }
}
