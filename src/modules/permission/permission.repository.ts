import { EntityRepository } from "typeorm";
import { BaseRepository } from "../../core/base.repository";
import { Permission } from "./permission.entity";

@EntityRepository(Permission)
export class PermissionRepository extends BaseRepository<Permission> {
  protected tableName = "permission";

  async getPermissionByCode(code: string): Promise<Permission> {
    return this.q().where("permission.code = :code", { code }).getOne();
  }
}
