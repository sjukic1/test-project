import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { AppConfigService } from "./core/appConfig/appConfig.service";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bodyParser: true });
  const appConfig = app.get(AppConfigService);
  app.setGlobalPrefix("api");

  app.useGlobalPipes(new ValidationPipe());

  app.enableCors({
    origin: (origin, cb) => {
      if (appConfig.cors.allowedOrigins.indexOf(origin) > -1 || true) {
        return cb(null, origin);
      }
    },
  });

  await app.listen(process.env.PORT || 4000, () => {
    if (process.env.ENV_NAME === "a") {
      console.log("🚀 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 🚀");
      console.log("🚀      App successfully started at     🚀");
      console.log("🚀        http://localhost:4000/api     🚀");
      console.log("🚀 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 🚀");
    }
  });
}
bootstrap();
