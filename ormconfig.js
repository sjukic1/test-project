/* eslint-disable @typescript-eslint/no-var-requires */
const loadEnv = (env) => require("dotenv").config({ path: `${env}` });
loadEnv(`.env.${process.env.ENV_NAME}`);
loadEnv(`.env.local`);
const config = require("config");

module.exports = {
  type: "postgres",
  ...config.get("dbConfig"),
  entities: ["dist/**/*.entity{.ts,.js}"],
  migrations: ["migrations/[1234567890]*.ts"],
  synchronize: false,
  cli: {
    migrationsDir: "migrations",
  },
  logging: true,
};
