## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

The package manager that I use in this project is yarn, be free to use another

To install all dependencies that are needed for this project run the command below.

```bash
$ yarn
```

The database that I used is Postgres which is started through Docker.

To run the below command you have to be in the root folder of this project (TEST-PROJECT).

```bash
# start database on docker
$ docker-compose up
```

Database is running on port 5435.
After this step, we need to run migration using this command.

## Running migrations

```bash
# run all migrations
$ yarn db:migrate

# or we can run the next command to drop everything and run migration
$ yarn db:reset:a

```

Then it is time to start the app.

## Running the app

```bash
# development
$ yarn start:a

```

Postman collection is located inside the project with examples of all routes that I implemented.
If you have any questions, please be free to reach out.
